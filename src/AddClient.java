import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Alex on 19/1/15.
 */
public class AddClient extends JFrame {
    private static dbConnection con = new dbConnection();
    private static JPanel contentPane;
    private static JButton btnAddClient;
    private static JLabel title, nameLabel, lastnameLabel;
    private static JTextField nameText, lastnameText;
    private static BufferedImage imageFile;

    public AddClient() {
        setBounds(100, 100, 100, 100);
        contentPane = new JPanel();
        this.add(contentPane);

        title = new JLabel("CREATE NEW CLIENT");
        title.setBounds(200,0,0,0);
        contentPane.add(title);

        nameLabel = new JLabel("Name: ");
        nameText = new JTextField();
        //nameLabel.setBounds(150, 120, 300, 300);
        //nameText.setBounds(180, 120, 100, 100);
        nameText.setColumns(50);
        contentPane.add(nameLabel);
        contentPane.add(nameText);

        lastnameLabel = new JLabel("LastName: ");
        lastnameText = new JTextField();
        lastnameLabel.setBounds(150,140,100,100);
        lastnameText.setBounds(180,140,100,100);
        contentPane.add(lastnameLabel);
        contentPane.add(lastnameText);

        btnAddClient = new JButton("Add the Client");
        btnAddClient.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                insertClient();
                contentPane.setVisible(false);
                new Main().setVisible(true);
            }
        });

        btnAddClient.setBounds(97, 43, 118, 23);
        contentPane.add(btnAddClient);

        title.setVisible(true);
        nameLabel.setVisible(true);
        nameText.setVisible(true);
        lastnameLabel.setVisible(true);
        lastnameText.setVisible(true);
        btnAddClient.setVisible(true);
        contentPane.setVisible(true);
    }

    private static void askForImage() {
        try {
            JFileChooser fileOpen = new JFileChooser();
            int returnVal = fileOpen.showOpenDialog(fileOpen);
            File file = fileOpen.getSelectedFile();
            imageFile = ImageIO.read(new File(file.getAbsolutePath()));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void insertClient() {
        con.insertClient(nameText.getText(), lastnameText.getText(), "null");
    }
}
