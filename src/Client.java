import java.util.Base64;

/**
 * Created by Alex on 18/1/15.
 */
public class Client {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Client(int id, String name, String lastname, String image) {

        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.image = image;
    }

    private int id;
    private String name;
    private String lastname;
    private String image;
}
