import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Alex on 18/1/15.
 */
public class dbConnection {
    public static Connection connection;
    private static Statement s;

    public dbConnection() {
        try {
            //Registrar driver MySQL
            DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());

            //Connexio a la BBDD
            connection = DriverManager.getConnection("jdbc:mysql://localhost/client", "root", null);

            //Crear Statement per a realitzar consultes
            s = connection.createStatement();
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static ArrayList<Client> showDB() {
        Client c;
        ArrayList<Client> array = new ArrayList<Client>();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from client");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                String name = (String) rs.getObject("name");
                String lastname = (String) rs.getObject("lastname");
                String image = (String) rs.getObject("image");

                c = new Client(id, name, lastname, image);
                array.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return array;
    }

    public static void insertClient(String name, String lastname, String imageEncoded) {
        // the mysql insert statement
        String query = " insert into client (name, lastname, image)"
                + " values (?, ?, ?)";

        // create the mysql insert preparedstatement
        try {
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, name);
            preparedStmt.setString(2, lastname);
            preparedStmt.setString(3, imageEncoded);

            // execute the preparedstatement
            preparedStmt.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
