import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Alex on 18/1/15.
 */
public class Main extends JFrame {
    private static dbConnection con = new dbConnection();
    private static JPanel contentPane;
    private static JTextArea textArea;
    private static JButton btnShowDB;
    private static JButton btnExit;
    private static JButton btnAddClient;
    private static JButton askForImage;
    private static BufferedImage image;
    private static JLabel imageLabel;

    public Main() {
        // boto nova venta configuracio , preparacio event I crida a seguent pantalla de la classe AltesFrame
        setBounds(100, 100, 450, 221);
        contentPane = new JPanel();
        this.add(contentPane);

        textArea = new JTextArea(5, 20);
        JScrollPane scrollPane = new JScrollPane(textArea);
        textArea.setEditable(false);
        contentPane.add(textArea);

        btnShowDB = new JButton("Show DB");
        btnShowDB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                showDB();
            }
        });

        btnShowDB.setBounds(100, 100, 118, 23);
        contentPane.add(btnShowDB);

        /*askForImage = new JButton("Image");
        askForImage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                askForImage();
                imageLabel = new JLabel(new ImageIcon(image));
                imageLabel.setBounds(97,43,40,40);
                contentPane.add(imageLabel);
                imageLabel.setVisible(true);
            }
        });

        askForImage.setBounds(97, 43, 118, 23);
        contentPane.add(askForImage);*/

        btnAddClient = new JButton("Add new Client");
        btnAddClient.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                contentPane.setVisible(false);
                new AddClient().setVisible(true);
            }
        });

        btnAddClient.setBounds(97, 43, 118, 23);
        contentPane.add(btnAddClient);

        btnExit = new JButton("Exit");
        btnExit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                System.exit(0);
            }
        });

        btnExit.setBounds(97, 43, 118, 23);
        contentPane.add(btnExit);

        textArea.setVisible(true);
        btnShowDB.setVisible(true);
        btnAddClient.setVisible(true);
        btnExit.setVisible(true);
        contentPane.setVisible(true);
    }

    private static void showDB() {
        //Es posa a nul per a que no surti el text repetit
        textArea.setText(null);
        String newline;
        ArrayList<Client> array = con.showDB();
        Client c;

        for (int i = 0; i < array.size(); i++) {
            c = array.get(i);
            newline = ("Id " + c.getId() + ": " + c.getName() + " " + c.getLastname());

            if (i == 0) {
                textArea.append(newline);
            } else {
                textArea.append("\n" + newline);
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        try {
            Main frame = new Main();
            frame.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
